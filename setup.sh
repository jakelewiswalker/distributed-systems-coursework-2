#!/bin/bash
chmod u+x run.bash
clear
echo "-------------------------"
echo "Setting up services"
echo "-------------------------"

# Set up venv
echo "Setting up venv folder..."
module add python
python3 -m venv venv
source venv/bin/activate

echo "Installing dependencies..."
pip install Flask
pip install re
pip install json
pip install requests
pip install os
pip install flask_restful
pip install flask_sqlalchemy
pip install flask_migrate

echo "Initialising database..."
cd wong_ws
python init_db.py

echo "Setup complete!"
