## Instructions
1. Setup database by running init_db.py
2. Run API by running run.py

**API usage**
- Requests are made to http://localhost:5002/film/<title>
- <title> is comprised of the name of the film
- Upon a successful request, the film name and filming location will be returned in a JSON format
- If a film is not found, and error message is returned
