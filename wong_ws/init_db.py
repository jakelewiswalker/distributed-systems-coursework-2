import os
from app import db
from app.models import Film, Location

# Initialise data
locations = ["Austin,TX", "Lone Pine", "Chateau Marmont", "Barstow,CA", "Telluride,CO", "Brandenburg Province", "Shinjuku,Tokyo", "Los Angeles County,CA"]
films = [{"title":"Death Proof", "location_id":1}, {"title":"Django Unchained", "location_id":2}, {"title":"Four Rooms", "location_id":3}, {"title":"From Dusk Till Dawn", "location_id":4}, {"title":"Hateful Eight", "location_id":5}, {"title":"Inglorious Bastards", "location_id":6}, {"title":"Kill Bill Vol 1", "location_id":7}, {"title":"Kill Bill Vol 2", "location_id":7}, {"title":"Jackie Brown", "location_id":8}, {"title":"Pulp Fiction", "location_id":8}, {"title":"Reservoir Dogs", "location_id":8}, {"title":"True Romance", "location_id":8}]

# Remove if already existing
if os.path.exists('film_locations.db'):
    os.remove('film_locations.db')

# Create database
db.create_all()

# Populate database
for location_name in locations:
    l = Location(name=location_name)
    db.session.add(l)

for film in films:
    f = Film(title=film["title"], location_id=film["location_id"])
    db.session.add(f)

db.session.commit()
