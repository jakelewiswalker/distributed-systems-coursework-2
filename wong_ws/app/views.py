from flask import Flask
from flask_restful import Resource, Api
from app import app, db
from app.models import Film, Location

api = Api(app)

class FilmLocation(Resource):
    def get(self, title):

        # Find film and corresponding data
        f = Film.query.filter(db.func.lower(Film.title)==title.lower()).first()
        
        # Error handling
        if f == None:
            return {"error": "No such film found"}, 404

        # Return result
        result = {"result": {
                    "film": f.title,
                    "location": f.location.name
                    }
                }, 200
        return result

api.add_resource(FilmLocation, "/film/<title>")
