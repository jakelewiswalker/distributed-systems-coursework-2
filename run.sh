#!/bin/bash
chmod u+x run.bash
clear
echo "-------------------------"
echo "Running all flask services"
echo "-------------------------"

module add python
source venv/bin/activate

python Walker/run.py &
python External/run.py &
python wong_ws/run.py &
