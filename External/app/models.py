from app import db

class Scripts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500), index=True, unique=False)
    script = db.Column(db.String(500000), index=True, unique=False)
  
