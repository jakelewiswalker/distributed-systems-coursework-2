import os
import json
import requests
from time import time
from app import app
from flask import Flask, request, render_template
from flask_restful import Resource, Api

api = Api(app)

@app.route('/', methods=['GET', 'POST'])
def index():
    location = None
    filmtitle = None

    if request.method == 'POST': #this block is only entered when the form is submitted
        response = queryFilm(request.form['query'])
        if response == None:
            location = "ERROR"
            filmtitle = "ERROR: Film not found"
        else:
            location = response['location']
            filmtitle = response['film']

    return render_template('index.html', locationpass = location, film = filmtitle)

def queryFilm(query):
    t0 = time()
    data = {"input" : query} #WEB SERVICE 1
    response = requests.get(
        url="http://0.0.0.0:5001",
        json=data
    )
    t1 = time()
    if response.status_code == 406:
        return None

    output = response.json() #WEB SERVICE 2
    output['movie']
    response2 = requests.get(url="http://0.0.0.0:5002/film/" + output['movie'])
    if response2.status_code == 404:
        return None
        
    t2 = time()
    print ('function service1 takes %f' %(t1-t0))
    print ('function service2 takes %f' %(t2-t1))
    return(response2.json()['result'])
