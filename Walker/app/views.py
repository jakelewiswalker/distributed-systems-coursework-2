import os
import re
import json
from app import app, db, models
from flask import Flask, request
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy

# Usage:
# curl -H "Content-Type: application/json" -X POST -d '{"input":"As Julia screams down to them Arlene hurries across the street and screams up to herin"}' http://127.0.0.1:5000/

api = Api(app)

def readfile(filename):
    with open(filename, 'r') as file:
        data = file.read().replace('\n', ' ')
    data = ''.join(e for e in data if e.isalnum()) #Filter out special characters etc in data
    return data

@app.route('/initdb')
def index():
    models.Scripts.query.delete()

    film1 = models.Scripts(name="Death Proof", script=readfile("tarantino/death-proof.txt"))
    film2 = models.Scripts(name="Django Unchained", script=readfile("tarantino/django-unchained.txt"))
    film3 = models.Scripts(name="Four Rooms", script=readfile("tarantino/four-rooms.txt"))
    film4 = models.Scripts(name="From Dusk Till Dawn", script=readfile("tarantino/from-dusk-till-dawn.txt"))
    film5 = models.Scripts(name="Hateful Eight", script=readfile("tarantino/the-hateful-eight.txt"))
    film6 = models.Scripts(name="Inglorious Bastards", script=readfile("tarantino/inglourious-bastards.txt"))
    film7 = models.Scripts(name="Kill Bill Vol 1", script=readfile("tarantino/kill-bill-vol-1.txt"))
    film8 = models.Scripts(name="Kill Bill Vol 2", script=readfile("tarantino/kill-bill-vol-2.txt"))
    film9 = models.Scripts(name="Jackie Brown", script=readfile("tarantino/jackie-brown.txt"))
    film10 = models.Scripts(name="Pulp Fiction", script=readfile("tarantino/pulp-fiction.txt"))
    film11 = models.Scripts(name="Reservoir Dogs", script=readfile("tarantino/reservoir-dogs.txt"))
    film12 = models.Scripts(name="True Romance", script=readfile("tarantino/true-romance.txt"))

    db.session.add(film1)
    db.session.add(film2)
    db.session.add(film3)
    db.session.add(film4)
    db.session.add(film5)
    db.session.add(film6)
    db.session.add(film7)
    db.session.add(film8)
    db.session.add(film9)
    db.session.add(film10)
    db.session.add(film11)
    db.session.add(film12)
    db.session.commit() 
    return "Successfully initialised database"

@app.route('/outputdb')
def output():
    database = "<h1>Loaded Film Scripts</h1>"
    for p in models.Scripts.query.all():
        database += "<p>" + p.script + "</p>"
    return database

class FindScript(Resource):
    def get(self):
        query = request.get_json(force=True)
        output = query['input']
        output = ''.join(e for e in output if e.isalnum()) #Get rid of spaces and special characters
        scripts = models.Scripts.query.all()
        for p in scripts:
            if str(output) in str(p.script):
                return{'movie': p.name}, 201

        return{'ERROR': 'UNABLE TO FIND'}, 406
        
# Usage:
# curl -H "Content-Type: application/json" -X POST -d '{"input":"As Julia screams down to them Arlene hurries across the street and screams up to herin"}' http://127.0.0.1:5000/

api.add_resource(FindScript, '/')

        