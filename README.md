![Logo](/External/app/static/rsz_tarantino.png)

## Web Services Coursework - [Tarantino Set Finder]
**Overview**
1. Input a film quote into Web Service 1 which returns the film name.
2. Input the film name into Web Service 2 which returns the film's filming location.
3. Input the filming location into the [Google Maps Platform](https://cloud.google.com/maps-platform/) to display the location on Google Maps.

**Setup**
1. Open a terminal window and navigate to the root of the project
2. Run ```chmod 755 setup.sh``` *if it is your first time running the code*
3. Then use ```./setup.sh``` to download all required dependencies
4. again run to give permissions to run the startup file ```chmod 755 run.sh```
5. Finally run ```./run.sh``` to begin the project

**Common issue**
If you have closed down the project without using ''Control + C'' then you will have to manually end the processes in the task manager as the port numbers will still have flask running on them and will not allow you to run over that socket/port.
